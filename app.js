
const express = require('express')
const app = express()
const port = 3000

/**
 * A very simple function to add two numbers together
 * @param {Number} a first param
 * @param {Number} b second param
 * @returns {Number} returns sum of a+b
 */
const add = (a,b) => {
    return a+b;
}


app.get('/', (req, res) => {
  res.send('Hello World!')
})

app.listen(port, () => {
  console.log(`Server listening on localhost:${port}`)
})
