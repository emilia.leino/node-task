# Node Task

Read me **first**!

## Installation steps
1. Clone repository with `git clone`
2. Read the manual
3. Take a drink
   
   
> *The most important step one can take is always the next one.*

![kitten](cat.jpg)

